// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "Tank.h"



void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	auto PlayerTank = Cast<ATank>(GetPawn());
	auto ControlTank = Cast<ATank>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (PlayerTank)
	{
		// Aim at the player
		PlayerTank->AimAt(ControlTank->GetActorLocation());
		// Fire if ready 
		PlayerTank->Fire(); // TODO don't fire every frame
	}
}

