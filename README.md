# BattleTank for UE4 Tutorial

Simple battle tank open-world head-to-head game with simple AI, terrain, and advanced control schemes.

## Authors

* **Rick de Klerk** - *Course student*

## Acknowledgments

* Based on tutorials from [Udemy](https://www.udemy.com/unrealcourse)